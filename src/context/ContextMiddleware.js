import React, { useState, useEffect } from 'react';
import GlobalContext from '../context/GlobalContext';
import { Toaster } from 'react-hot-toast';

const ContextMiddleware = (props) => {
    const [contextState, setContextState] = useState({
        token: '',
        personId:"",
        fullName:'',
        photo:"",
        isAdmin: false,
    });

    const getLocalCache = () => {
        let localContextCached = localStorage.getItem("localContext");
        return localContextCached === null ? null : JSON.parse(localContextCached);
    };

    useEffect(() => {
        const localContextCached = getLocalCache();
        if (localContextCached !== null) {
            setContextState(localContextCached);
        }
    }, []);

    const middleware = (state, setState) => {
        let localContext = Object.assign({}, { ...state });

        const setLocalCache = (localContextCached) => {
            localStorage.setItem("localContext", JSON.stringify(localContextCached));
            setState(localContextCached);
        };

        const signIn = (token) => {
            localContext = Object.assign(
                {},
                { ...localContext },
                { token }
            );
            setLocalCache(localContext);
        };

        const newToken = (newToken,role,id) =>{
            localContext = Object.assign(
                {},
                { ...localContext },
                {  token: newToken, isAdmin: role, personId:id}
            );
            setLocalCache(localContext);
        };

        const signOut = () => {
            localContext = Object.assign(
                {},
                { ...localContext },
                { token: '', fullName:'', isAdmin:'', photo:'', personId:"" }
            );
            setLocalCache(localContext);
        };

           const newUser = (fullName, photo) =>{
            localContext = Object.assign(
                {},
                { ...localContext },
                {  fullName: fullName, 
                    photo:photo
                }
            );
            setLocalCache(localContext);
        };

        return {signIn, signOut, newToken, newUser};
    };

    return (
        <GlobalContext.Provider
            value={[
                contextState,
                setContextState,
                middleware(contextState, setContextState),
            ]}
        >

        <Toaster position="top-center"
            reverseOrder={false}
            gutter={8}
            containerClassName=""
            containerStyle={{}}
            toastOptions={{
                // Define default options
                className: '',
                duration: 5000,
                style: {
                background: '#627485',
                color: '#fff',
                },
                // Default options for specific types
                success: {
                duration: 3000,
                theme: {
                    primary: 'green',
                    secondary: 'black',
                },
                },
            }}/>
            {props.children}    
        </GlobalContext.Provider>
    );
};

export default ContextMiddleware;
