export const  personById = async (id) => {
    const body = {id};
    return fetch(`${process.env.REACT_APP_API}person/personbyid`, {
        method: "POST",
         body: JSON.stringify(body),
        headers: {
            "Content-Type": "application/json",
        }   
    })
}

export const allPerson = async () => {
    return fetch(`${process.env.REACT_APP_API}person/allperson`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            Authorization: 'Bearer',
        }   
    })
}

// export  const locationPerson = async (personid) => {
//     const body = {personid};
//     return fetch("http://localhost:4000/api/v1/person/locationperson", {
//         method: "POST",
//          body: JSON.stringify(body),
//         headers: {
//             "Content-Type": "application/json",
//             Authorization: 'Bearer',
//         }   
//     })
// }

