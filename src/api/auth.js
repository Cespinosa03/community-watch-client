const signIn = async (userName, password) => {
    const body = {userName, password};
    return fetch(`${process.env.REACT_APP_API}auth/signin`, {
        method: "POST",
        headers:{"Content-Type": "application/json"},
        body: JSON.stringify(body)
    })
}



module.exports = {
    signIn
};