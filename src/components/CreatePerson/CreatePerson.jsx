import React, { useState } from 'react';
import CreatePersonForm from '../CreatePerson/CreatePersonForm';
import InfPersonalForm from '../CreatePerson/InfPersonalForm';
import InfLocationForm from '../CreatePerson/InfLocationForm ';
import InfVehicleForm from '../CreatePerson/InfVehicleForm';

const CreatePerson = () => {

    const [tag, setTag] = useState('personal')
    const [tittle, setTittle] = useState('Personal Information')

    const tagChange = (tagValue) => {
        switch (tagValue) {
            case 'personal':
                setTag(<InfPersonalForm />)
                setTittle('Personal Information');
                break;
            case 'location':
                setTag(<InfLocationForm />)
                setTittle('Location Information');
                break;
            case 'vehicle':
                setTag(<InfVehicleForm />)
                setTittle('Vehicle Information');
                break;
            default:
                <InfPersonalForm />
                break;
        }
    }
    return (
        <>
            <CreatePersonForm
                tagChange={tagChange}
                tag={tag}
                tittle={tittle}
            />
        </>
    )
}

export default CreatePerson;