import React from 'react';

const CreatePersonForm = ({ tagChange, tag, tittle }) => {

    return (
        <>
            <div className="d-flex justify-content-center">

                <div className="tag-container">
                    <div className="d-flex justify-content-center createperson-tittle">{tittle}</div>
                    <div className="col-10 tag-menu d-flex justify-content-between">
                        <div>
                            <button className="tag-style" onClick={() => tagChange('personal')}>Personal Information</button>
                        </div>
                        <div>
                            <button className="tag-style" onClick={() => tagChange('location')}>Location Information</button>
                        </div>
                        <div>
                            <button className="tag-style" onClick={() => tagChange('vehicle')}>Vehicle Information</button>
                        </div>
                        {/* <div>
                            <button className="tag-style">Create User</button>
                        </div> */}
                    </div>
                    <div className="col-10 main-container">
                        <div className="">
                            {tag}
                        </div>
                    </div>
                    <div className="col-10 d-flex justify-content-around m-auto mt-4">
                        <button className="btn-login" name="btn-submit" type="submit" onClick="{handeleSignIn}">Create</button>
                        <button className="btn-cancel" name="btn-submit" type="submit" onClick="{handeleSignIn}">Cancel</button>
                    </div>

                </div>


            </div>

        </>
    )
}

export default CreatePersonForm;