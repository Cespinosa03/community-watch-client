import React from 'react'

const InfVehicleForm = () => {
    return (
        <>
            <div class="row">
                <div className="col">
                    <input
                        className="createperson-input"
                        name="type"
                        type="text"
                        placeholder="Type"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    <input
                        className="createperson-input"
                        name="brand"
                        type="text"
                        placeholder="Brand"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    <input
                        className="createperson-input"
                        name="model"
                        type="text"
                        placeholder="Model"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>

                <div className="w-100 mt-5"></div>

                <div className="col">
                    <input
                        className="createperson-input"
                        name="color"
                        type="text"
                        placeholder="Color"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    <input
                        className="createperson-input"
                        name="year"
                        type="text"
                        placeholder="Year"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    <input
                        className="createperson-input"
                        name="licenseplate"
                        type="number"
                        placeholder="License Plate"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>

                <div className="w-100 mt-5"></div>

                <div className="col">
                    <input
                        className="createperson-input"
                        name="rfid"
                        type="text"
                        placeholder="RFID"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    {/* <input
                        className="createperson-input"
                        name="rfid"
                        type="text"
                        placeholder="RFID"
                        minLength="4"
                        maxLength="16"
                        required
                    /> */}
                </div>
                <div className="col">
                    {/* <input
                        className="createperson-input"
                        name="rfid"
                        type="text"
                        placeholder="RFID"
                        minLength="4"
                        maxLength="16"
                        required
                    /> */}
                </div>
            </div>
        </>
    )
}

export default InfVehicleForm;
