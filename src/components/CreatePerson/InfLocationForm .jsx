import React from 'react'

const InfLocationForm = () => {
    return (
        <>
            <div class="row">
                <div className="col">
                    <input
                        className="createperson-input"
                        name="country"
                        type="text"
                        placeholder="Country"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    <input
                        className="createperson-input"
                        name="city"
                        type="text"
                        placeholder="City"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    <input
                        className="createperson-input"
                        name="typeofhousing"
                        type="text"
                        placeholder="Type Of Housing"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>

                <div className="w-100 mt-5"></div>

                <div className="col">
                    <input
                        className="createperson-input"
                        name="street"
                        type="text"
                        placeholder="Street"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    <input
                        className="createperson-input"
                        name="block"
                        type="text"
                        placeholder="Block"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    <input
                        className="createperson-input"
                        name="Housenumber"
                        type="number"
                        placeholder="House Number"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
            </div>
        </>
    )
}

export default InfLocationForm;
