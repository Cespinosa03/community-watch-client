import React from 'react'

const InfPersonalForm = () => {
    return (
        <>
            <div class="row">
                <div className="col">
                    <input
                        className="createperson-input"
                        name="firstname"
                        type="text"
                        placeholder="First Name"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    <input
                        className="createperson-input"
                        name="lastname"
                        type="text"
                        placeholder="Last Name"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    <input
                        className="createperson-input"
                        name="documentid"
                        type="text"
                        placeholder="Document ID"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>

                <div className="w-100 mt-5"></div>

                <div className="col">
                    <input
                        className="createperson-input"
                        name="phonenumber"
                        type="text"
                        placeholder="Phone Number"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    <input
                        className="createperson-input"
                        name="cellnumber"
                        type="text"
                        placeholder="Cell Number"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    <input
                        className="createperson-input"
                        name="email"
                        type="text"
                        placeholder="Email"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
            </div>
        </>
    )
}

export default InfPersonalForm;
