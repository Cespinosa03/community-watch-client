import React from "react";
import { Link } from "react-router-dom";
import Images from "../../common/images";

const DashboardForm = () => {
  return (
    <>
      <div className="dashboard-container">
        <div className="dashboard-btns">
          <div className="dashboard-btn">
            <Link to="./paysecurity" style={{ textDecoration: "none" }}>
              <div className="">
                <img
                  className="dashboard-btn-img"
                  src={Images.iconPay}
                  alt=""
                />
                <p className="dashboard-btn-txt">Cobrar Mensualidad</p>
              </div>
            </Link>
          </div>
          <div className="dashboard-btn">
            <Link to="./paysecurity" style={{ textDecoration: "none" }}>
              <div className="">
                <img
                  className="dashboard-btn-img"
                  src={Images.iconPayCar}
                  alt=""
                />
                <p className="dashboard-btn-txt">Otros Pagos</p>
              </div>
            </Link>
          </div>
          <div className="dashboard-btn">
            <Link to="./paysecurity" style={{ textDecoration: "none" }}>
              <div className="">
                <img className="dashboard-btn-img" src={Images.report} alt="" />
                <p className="dashboard-btn-txt">Reportes</p>
              </div>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default DashboardForm;
