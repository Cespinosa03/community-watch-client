import React from "react";
import DashboardForm from "./DashboardForm";

const Dashboard = () => {
  return (
    <>
      <DashboardForm />
    </>
  );
};

export default Dashboard;
