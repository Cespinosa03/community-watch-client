import React, { useState, useContext, useRef } from "react";
import GlobalContext from "../../context/GlobalContext";
import { signIn } from "../../api/auth";
import { personById } from "../../api/person";
import LoginForm from "./LoginForm";
import toast from "react-hot-toast";

const Login = () => {
  const [, , contextMiddleware] = useContext(GlobalContext);

  const [profile, setProfile] = useState({
    username: "",
    password: "",
  });

  const loginBtn = useRef();

  const handleInputChange = (e) => {
    setProfile({
      ...profile,
      [e.target.name]: e.target.value,
    });
  };

  const clickLogin = (event) => {
    if (event.key === "Enter") {
      loginBtn.current.click();
    }
  };

  const handeleSignIn = (e) => {
    e.preventDefault();

    if (profile.username === "") {
      return toast.error("Por favor de digitar un nombre de usuario");
    } else if (profile.password === "") {
      return toast.error("Por favor de digitar su contraseña");
    }

    signIn(profile.username, profile.password)
      .then((res) => {
        if (res.status > 400) {
          return toast.error("Usuario Incorrecto!");
        } else {
          return res.json();
        }
      })
      .then((res) => {
        contextMiddleware.newToken(
          res.token,
          res.user.rolename === "admin" ? true : false,
          res.user.personid
        );
        personById(res.user.personid)
          .then((res) => res.json())
          .then((res) =>
            contextMiddleware.newUser(
              res.name.first.split(" ")[0] + " " + res.name.last.split(" ")[0],
              res.photo
            )
          )
          .catch((err) => console.log(err.message));
      })
      .catch((err) => {
        console.log(err.status);
      });
  };

  return (
    <>
      <LoginForm
        profileInputs={handleInputChange}
        handeleSignIn={handeleSignIn}
        profile={profile}
        clickLogin={clickLogin}
        loginBtn={loginBtn}
      />
    </>
  );
};

export default Login;
