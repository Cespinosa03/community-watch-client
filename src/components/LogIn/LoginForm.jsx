import React from "react";
import Images from "../../common/images";

const LoginForm = ({
  profileInputs,
  handeleSignIn,
  profile,
  modalToggle,
  clickLogin,
  loginBtn,
}) => {
  return (
    <>
      <div className="container">
        <figure className="d-flex justify-content-center">
          <img className="img-login" src={Images.login} alt="" />
        </figure>
        <div className="d-flex justify-content-center mb-5">
          <p className="txt-login">Iniciar Seccion</p>
        </div>

        <div className="row mt-3">
          <form
            className=""
            name="loginForm"
            action=""
            method="post" /*onSubmit={handeleSignIn}*/
          >
            <div className="d-flex justify-content-center mb-4">
              <input
                name="username"
                type="text"
                className="input-login"
                placeholder="Entre su nombre de usuario"
                required
                onChange={profileInputs}
                value={profile.username.toUpperCase()}
              />
            </div>
            <div className="d-flex justify-content-center">
              <input
                name="password"
                type="password"
                className="input-login"
                placeholder="Entre su Contraseña"
                required
                onChange={profileInputs}
                value={profile.password}
                onKeyPress={clickLogin}
              />
            </div>
            <div className="d-flex justify-content-center mt-2">
              <p className="txt-forgot">Olvido su contraseña?</p>
            </div>
            <div className="d-flex justify-content-center mt-4">
              <button
                className="btn-login"
                name="btn-submit"
                type="submit"
                onClick={handeleSignIn}
                ref={loginBtn}
              >
                Logearse
              </button>
            </div>
            <div className="d-flex justify-content-center mt-4">
              <button
                className="btn-create"
                name="btn-create"
                type="button"
                onClick={modalToggle}
              >
                Crear Usuario
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default LoginForm;
