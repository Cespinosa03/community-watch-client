import React, { useEffect, useState } from "react";
import PaySecurityForm from "./PaySecurityForm";
import { allPerson } from "../../api/person";
import { getInformation } from "../../api/paysecurity";
import toast from "react-hot-toast";

const PaySecurity = () => {
  const [person, setPerson] = useState("");
  const [fullName, setfullName] = useState("");
  const [information, SetInformation] = useState({
    fullName: "",
    documentId: "",
    phoneNumber: "",
    celNumber: "",
    email: "",
    typeOfHousing: "",
    street: "",
    house: "",
  });

  useEffect(() => {
    let unmounted = false;

    allPerson()
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        if (!unmounted) {
          setPerson(res);
        }
      })
      .catch((err) => {
        console.error(err.status);
      });

    return () => {
      unmounted = true;
    };
  }, []);

  // console.log(person);

  const fillContentInf = (type) => {
    if (!fullName && type === "name") {
      return toast.error("Campo de nombre vacio!");
    } else if (!fullName && type === "document") {
      return toast.error("Campo de cedula vacio!");
    }

    var found = person?.filter((item) => item.name.full === fullName);

    console.log(found);

    getInformation(found._id)
      .then((profile) => {
        return profile.json();
      })
      .then((profile) => {
        console.log(profile);
        // SetInformation({
        //   fullname: profile.profile.FullName,
        //   documentid: profile.profile.DocumentID,
        //   phonenumber: profile.profile.PhoneNumber,
        //   email: profile.profile.Email,
        //   street: profile.profile.Street,
        //   house: profile.profile.HouseNumber,
        // });
      })
      .catch((err) => {
        console.log(err.message);
      });
  };
  return (
    <PaySecurityForm
      setfullName={setfullName}
      person={person}
      fillContentInf={fillContentInf}
    />
  );
};

export default PaySecurity;
