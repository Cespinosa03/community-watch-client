import React from "react";
import { MdDoubleArrow, MdPayment } from "react-icons/md";
import { FiEdit } from "react-icons/fi";
import { HiSearchCircle } from "react-icons/hi";
const PaySecurity = ({ setfullName, person, fillContentInf }) => {
  return (
    <>
      <div className="paysecurity-container">
        <div className="paysecurity-grid">
          <div className="paysecurity-seach">
            <div className="payseacurity-title-search-text">
              <p className="">BUSCAR RESIDENTE</p>
            </div>

            <div className="">
              <input
                className="payseacurity-input-search"
                type="text"
                placeholder="Buscar por nombre"
                list="name"
                onChange={(e) => setfullName(e.target.value)}
              />
              <datalist id="name">
                {person
                  ? person?.map((user) => {
                      return (
                        <option
                          key={user._id}
                          id={user._id}
                          value={user.name.full}
                        ></option>
                      );
                    })
                  : null}
              </datalist>

              <i className="hi hi-search-circle" />
              <HiSearchCircle
                onClick={() => fillContentInf("name")}
                style={{
                  cursor: "pointer",
                  marginBottom: "0.4rem",
                  marginLeft: "0.5rem",
                }}
                size="2rem"
                color="white"
              />
            </div>
            <div>
              <input
                className="payseacurity-input-search"
                type="text"
                placeholder="Buscar por cedula"
                // list="name"
                onChange={(e) => setfullName(e.target.value)}
              />
              <i className="hi hi-search-circle" />
              <HiSearchCircle
                onClick={() => fillContentInf("document")}
                style={{
                  cursor: "pointer",
                  marginBottom: "0.4rem",
                  marginLeft: "0.5rem",
                }}
                size="2rem"
                color="white"
              />
            </div>
            {/* <div>
              <button className="btn payseacurity-button-search" type="button">
                Buscar
              </button>
            </div> */}
          </div>
          <div className="paysecurity-inf">
            <div className="payseacurity-title-inf-text">
              <p className="">INFORMACION DE RESIDENTE</p>
            </div>
            <div className="paysecurity-inf-grid">
              <div className="paysecurity-inf-grid-cont">
                <div className="paysecurity-inf-sention">
                  <div>
                    <div className="paysecurity-inf-subtitle">Nombre:</div>
                    <div className="paysecurity-inf-subtitle">Cedula:</div>
                    <div className="paysecurity-inf-subtitle">Telefono:</div>
                    <div className="paysecurity-inf-subtitle">Celular:</div>
                  </div>
                  <div>
                    <div className="paysecurity-inf-text">Nombre</div>
                    <div className="paysecurity-inf-text">Cedula</div>
                    <div className="paysecurity-inf-text">Telefono</div>
                    <div className="paysecurity-inf-text">Celular</div>
                  </div>
                </div>
              </div>
              <div className="paysecurity-inf-grid-cont">
                <div className="paysecurity-inf-sention">
                  <div>
                    <div className="paysecurity-inf-subtitle">Correo:</div>
                    <div className="paysecurity-inf-subtitle">Vivienda:</div>
                    <div className="paysecurity-inf-subtitle">Calle:</div>
                    <div className="paysecurity-inf-subtitle">Casa#:</div>
                  </div>
                  <div>
                    <div className="paysecurity-inf-text">Nombre</div>
                    <div className="paysecurity-inf-text">Tipo de Vivienda</div>
                    <div className="paysecurity-inf-text">Telefono</div>
                    <div className="paysecurity-inf-text">Numero de Casa</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="paysecurity-year">
            <p className="m-0">
              <i className="md md-double-arrow" />
              <MdDoubleArrow
                style={{ transform: "rotate(180deg)", cursor: "pointer" }}
                size="2rem"
                color="white"
              />
            </p>
            <p className="m-0">2022</p>

            <p className="m-0">
              <i className="md md-double-arrow" />
              <MdDoubleArrow
                style={{ cursor: "pointer" }}
                size="2rem"
                color="white"
              />
            </p>
          </div>
        </div>
        <div className="paysecurity-datagrip">
          <div className="paysecurity-datagrip-grid">
            {" "}
            <div className="paysecurity-datagrip-header">No.</div>
            <div className="paysecurity-datagrip-header">Mes</div>
            <div className="paysecurity-datagrip-header">Monto</div>
            <div className="paysecurity-datagrip-header">Pago</div>
            <div className="paysecurity-datagrip-header">Abono</div>
            <div className="paysecurity-datagrip-header">Pendiente</div>
            <div className="paysecurity-datagrip-header">Comentario</div>
            <div className="paysecurity-datagrip-header">Pagar</div>
            <div className="paysecurity-datagrip-header">Editar</div>
            {/* <div className="paysecurity-datagrip-header">
              <p className="m-0">
                <i className="md md-double-arrow" />
                <MdDoubleArrow
                  style={{ transform: "rotate(180deg)", cursor: "pointer" }}
                  size="1.5rem"
                  color="white"
                />
              </p>
              <p className="m-0">2022</p>

              <p className="m-0">
                <i className="md md-double-arrow" />
                <MdDoubleArrow
                  style={{ cursor: "pointer" }}
                  size="1.5rem"
                  color="white"
                />
              </p>
            </div> */}
          </div>
        </div>
        <div className="paysecurity-datagrip">
          <div
            style={{ backgroundColor: "#353a3f" }}
            className="paysecurity-datagrip-grid"
          >
            <div className="paysecurity-datagrip-cont">1</div>
            <div className="paysecurity-datagrip-cont">Enero</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">
              Click aqui para agregar un comentario...
            </div>
            <div className="paysecurity-datagrip-cont">
              {" "}
              <p className="m-0">
                <i className="md md-payment" />
                <MdPayment
                  style={{ cursor: "pointer" }}
                  size="2rem"
                  color="#52b7be"
                />
              </p>
            </div>
            <div className="paysecurity-datagrip-cont">
              <p className="m-0">
                <i className="fi fi-edit" />
                <FiEdit
                  style={{ cursor: "pointer" }}
                  size="1.8rem"
                  color="#B25068"
                />
              </p>
            </div>
            {/* <div
              style={{ justifyContent: "space-around" }}
              className="paysecurity-datagrip-cont"
            >
              <p className="m-0">
                <i className="md md-payment" />
                <MdPayment
                  style={{ cursor: "pointer" }}
                  size="2rem"
                  color="#52b7be"
                />
                Pagar
              </p>
              <p className="m-0">
                <i className="fi fi-edit" />
                <FiEdit
                  style={{ cursor: "pointer" }}
                  size="1.8rem"
                  color="#B25068"
                />
                Editar
              </p>
            </div> */}
          </div>
        </div>
        <div className="paysecurity-datagrip">
          <div className="paysecurity-datagrip-grid">
            <div className="paysecurity-datagrip-cont">1</div>
            <div className="paysecurity-datagrip-cont">Enero</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">
              Click aqui para agregar un comentario...
            </div>
            <div className="paysecurity-datagrip-cont">
              {" "}
              <p className="m-0">
                <i className="md md-payment" />
                <MdPayment
                  style={{ cursor: "pointer" }}
                  size="2rem"
                  color="#52b7be"
                />
              </p>
            </div>
            <div className="paysecurity-datagrip-cont">
              <p className="m-0">
                <i className="fi fi-edit" />
                <FiEdit
                  style={{ cursor: "pointer" }}
                  size="1.8rem"
                  color="#B25068"
                />
              </p>
            </div>
          </div>
        </div>
        <div
          style={{ backgroundColor: "#353a3f" }}
          className="paysecurity-datagrip"
        >
          <div className="paysecurity-datagrip-grid">
            <div className="paysecurity-datagrip-cont">1</div>
            <div className="paysecurity-datagrip-cont">Enero</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">
              Click aqui para agregar un comentario...
            </div>
            <div className="paysecurity-datagrip-cont">
              {" "}
              <p className="m-0">
                <i className="md md-payment" />
                <MdPayment
                  style={{ cursor: "pointer" }}
                  size="2rem"
                  color="#52b7be"
                />
              </p>
            </div>
            <div className="paysecurity-datagrip-cont">
              <p className="m-0">
                <i className="fi fi-edit" />
                <FiEdit
                  style={{ cursor: "pointer" }}
                  size="1.8rem"
                  color="#B25068"
                />
              </p>
            </div>
          </div>
        </div>
        <div className="paysecurity-datagrip">
          <div className="paysecurity-datagrip-grid">
            <div className="paysecurity-datagrip-cont">1</div>
            <div className="paysecurity-datagrip-cont">Enero</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">
              Click aqui para agregar un comentario...
            </div>
            <div className="paysecurity-datagrip-cont">
              {" "}
              <p className="m-0">
                <i className="md md-payment" />
                <MdPayment
                  style={{ cursor: "pointer" }}
                  size="2rem"
                  color="#52b7be"
                />
              </p>
            </div>
            <div className="paysecurity-datagrip-cont">
              <p className="m-0">
                <i className="fi fi-edit" />
                <FiEdit
                  style={{ cursor: "pointer" }}
                  size="1.8rem"
                  color="#B25068"
                />
              </p>
            </div>
          </div>
        </div>
        <div
          style={{ backgroundColor: "#353a3f" }}
          className="paysecurity-datagrip"
        >
          <div className="paysecurity-datagrip-grid">
            <div className="paysecurity-datagrip-cont">1</div>
            <div className="paysecurity-datagrip-cont">Enero</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">
              Click aqui para agregar un comentario...
            </div>
            <div className="paysecurity-datagrip-cont">
              {" "}
              <p className="m-0">
                <i className="md md-payment" />
                <MdPayment
                  style={{ cursor: "pointer" }}
                  size="2rem"
                  color="#52b7be"
                />
              </p>
            </div>
            <div className="paysecurity-datagrip-cont">
              <p className="m-0">
                <i className="fi fi-edit" />
                <FiEdit
                  style={{ cursor: "pointer" }}
                  size="1.8rem"
                  color="#B25068"
                />
              </p>
            </div>
          </div>
        </div>
        <div className="paysecurity-datagrip">
          <div className="paysecurity-datagrip-grid">
            <div className="paysecurity-datagrip-cont">1</div>
            <div className="paysecurity-datagrip-cont">Enero</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">
              Click aqui para agregar un comentario...
            </div>
            <div className="paysecurity-datagrip-cont">
              {" "}
              <p className="m-0">
                <i className="md md-payment" />
                <MdPayment
                  style={{ cursor: "pointer" }}
                  size="2rem"
                  color="#52b7be"
                />
              </p>
            </div>
            <div className="paysecurity-datagrip-cont">
              <p className="m-0">
                <i className="fi fi-edit" />
                <FiEdit
                  style={{ cursor: "pointer" }}
                  size="1.8rem"
                  color="#B25068"
                />
              </p>
            </div>
          </div>
        </div>
        <div
          style={{ backgroundColor: "#353a3f" }}
          className="paysecurity-datagrip"
        >
          <div className="paysecurity-datagrip-grid">
            <div className="paysecurity-datagrip-cont">1</div>
            <div className="paysecurity-datagrip-cont">Enero</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">
              Click aqui para agregar un comentario...
            </div>
            <div className="paysecurity-datagrip-cont">
              {" "}
              <p className="m-0">
                <i className="md md-payment" />
                <MdPayment
                  style={{ cursor: "pointer" }}
                  size="2rem"
                  color="#52b7be"
                />
              </p>
            </div>
            <div className="paysecurity-datagrip-cont">
              <p className="m-0">
                <i className="fi fi-edit" />
                <FiEdit
                  style={{ cursor: "pointer" }}
                  size="1.8rem"
                  color="#B25068"
                />
              </p>
            </div>
          </div>
        </div>
        <div className="paysecurity-datagrip">
          <div className="paysecurity-datagrip-grid">
            <div className="paysecurity-datagrip-cont">1</div>
            <div className="paysecurity-datagrip-cont">Enero</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">
              Click aqui para agregar un comentario...
            </div>
            <div className="paysecurity-datagrip-cont">
              {" "}
              <p className="m-0">
                <i className="md md-payment" />
                <MdPayment
                  style={{ cursor: "pointer" }}
                  size="2rem"
                  color="#52b7be"
                />
              </p>
            </div>
            <div className="paysecurity-datagrip-cont">
              <p className="m-0">
                <i className="fi fi-edit" />
                <FiEdit
                  style={{ cursor: "pointer" }}
                  size="1.8rem"
                  color="#B25068"
                />
              </p>
            </div>
          </div>
        </div>
        <div
          style={{ backgroundColor: "#353a3f" }}
          className="paysecurity-datagrip"
        >
          <div className="paysecurity-datagrip-grid">
            <div className="paysecurity-datagrip-cont">1</div>
            <div className="paysecurity-datagrip-cont">Enero</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">
              Click aqui para agregar un comentario...
            </div>
            <div className="paysecurity-datagrip-cont">
              {" "}
              <p className="m-0">
                <i className="md md-payment" />
                <MdPayment
                  style={{ cursor: "pointer" }}
                  size="2rem"
                  color="#52b7be"
                />
              </p>
            </div>
            <div className="paysecurity-datagrip-cont">
              <p className="m-0">
                <i className="fi fi-edit" />
                <FiEdit
                  style={{ cursor: "pointer" }}
                  size="1.8rem"
                  color="#B25068"
                />
              </p>
            </div>
          </div>
        </div>
        <div className="paysecurity-datagrip">
          <div className="paysecurity-datagrip-grid">
            <div className="paysecurity-datagrip-cont">1</div>
            <div className="paysecurity-datagrip-cont">Enero</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">
              Click aqui para agregar un comentario...
            </div>
            <div className="paysecurity-datagrip-cont">
              {" "}
              <p className="m-0">
                <i className="md md-payment" />
                <MdPayment
                  style={{ cursor: "pointer" }}
                  size="2rem"
                  color="#52b7be"
                />
              </p>
            </div>
            <div className="paysecurity-datagrip-cont">
              <p className="m-0">
                <i className="fi fi-edit" />
                <FiEdit
                  style={{ cursor: "pointer" }}
                  size="1.8rem"
                  color="#B25068"
                />
              </p>
            </div>
          </div>
        </div>
        <div
          style={{ backgroundColor: "#353a3f" }}
          className="paysecurity-datagrip"
        >
          <div className="paysecurity-datagrip-grid">
            <div className="paysecurity-datagrip-cont">1</div>
            <div className="paysecurity-datagrip-cont">Enero</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">
              Click aqui para agregar un comentario...
            </div>
            <div className="paysecurity-datagrip-cont">
              {" "}
              <p className="m-0">
                <i className="md md-payment" />
                <MdPayment
                  style={{ cursor: "pointer" }}
                  size="2rem"
                  color="#52b7be"
                />
              </p>
            </div>
            <div className="paysecurity-datagrip-cont">
              <p className="m-0">
                <i className="fi fi-edit" />
                <FiEdit
                  style={{ cursor: "pointer" }}
                  size="1.8rem"
                  color="#B25068"
                />
              </p>
            </div>
          </div>
        </div>
        <div className="paysecurity-datagrip">
          <div className="paysecurity-datagrip-grid">
            <div className="paysecurity-datagrip-cont">1</div>
            <div className="paysecurity-datagrip-cont">Enero</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">0</div>
            <div className="paysecurity-datagrip-cont">1,000</div>
            <div className="paysecurity-datagrip-cont">
              Click aqui para agregar un comentario...
            </div>
            <div className="paysecurity-datagrip-cont">
              {" "}
              <p className="m-0">
                <i className="md md-payment" />
                <MdPayment
                  style={{ cursor: "pointer" }}
                  size="2rem"
                  color="#52b7be"
                />
              </p>
            </div>
            <div className="paysecurity-datagrip-cont">
              <p className="m-0">
                <i className="fi fi-edit" />
                <FiEdit
                  style={{ cursor: "pointer" }}
                  size="1.8rem"
                  color="#B25068"
                />
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default PaySecurity;
