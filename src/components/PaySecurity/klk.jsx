import React, { useState, useEffect, useContext } from "react";
import DataGrip from "../../common/DataGrid";
import {
  getResidents,
  getInformation,
  getInformationID,
} from "../../api/paysecurity";
import { apiAllPerson, locationPerson } from "../../api/person";
import GlobalContext from "../../context/GlobalContext";

const Index = () => {
  const [contextState, , contextMiddleware] = useContext(GlobalContext);

  const [listResidents, setListResidents] = useState([]);
  const [fullName, setfullName] = useState("");
  const [documentID, setDocumentID] = useState("");
  const [callSearch, setCallSearch] = useState("");
  const [inputActive, setInputActive] = useState(true);

  const [information, SetInformation] = useState({
    fullname: "",
    documentid: "",
    phonenumber: "",
    email: "",
    street: "",
    house: "",
  });

  useEffect(() => {
    apiAllPerson()
      .then((resp) => {
        return resp.json();
      })
      .then((resp) => {
        console.log(resp);
        const list = resp.map(function (arr) {
          return arr;
        });

        setListResidents(list);
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, []);

  const fillContentInf = () => {
    if (documentID === "" && fullName === "") {
      return alert(
        "Por favor de llenar uno de los dos campos para realizar la busqueda"
      );
    }

    if (inputActive) {
      setDocumentID("");
      getInformation(fullName)
        .then((profile) => {
          return profile.json();
        })
        .then((profile) => {
          SetInformation({
            fullname: profile.profile.FullName,
            documentid: profile.profile.DocumentID,
            phonenumber: profile.profile.PhoneNumber,
            email: profile.profile.Email,
            street: profile.profile.Street,
            house: profile.profile.HouseNumber,
          });
        })
        .catch((err) => {
          console.log(err.message);
        });
    } else if (!inputActive) {
      setfullName("");
      locationPerson(documentID)
        .then((profile) => {
          return profile.json();
        })
        .then((profile) => {
          SetInformation({
            fullname: `${profile.FirstName} ${profile.LastName}`,
            documentid: profile.DocumentId,
            phonenumber: profile.PhoneNumber,
            email: profile.Email,
            street: profile.jdvresidentlocs[0].Street,
            house: profile.jdvresidentlocs[0].HouseNumber,
          });
        })
        .catch((err) => {
          console.log(err.message);
        });
    } else {
      console.error("please fill one field");
    }
  };

  return (
    <div className="main-container-login w100 h100" id="root">
      <div className="row col-12 mainmenu-container">
        <div className="col-4 securtiy-container-search">
          <p className="m-0 title-search">Buscar</p>
          <div className="p-2 justify-content-center row">
            <input
              className={`${
                inputActive ? "input-active" : "input-desactive"
              }  input-search`}
              type="text"
              placeholder="Nombre"
              list="name"
              onChange={(e) => setfullName(e.target.value)}
              onFocus={() => setInputActive(true)}
              value={inputActive ? null : ""}
            />

            <datalist id="name">
              {listResidents.map((user) => {
                return (
                  <option
                    key={user.PersonId}
                    value={`${user.FirstName} ${user.LastName}`}
                  ></option>
                );
              })}
            </datalist>
          </div>

          <div className="p-2 justify-content-center row">
            <input
              className={`${
                inputActive ? "input-desactive" : "input-active"
              }  input-search`}
              type="text"
              placeholder="Cedula"
              onChange={(e) => setDocumentID(e.target.value)}
              onFocus={() => setInputActive(false)}
              value={inputActive ? "" : null}
            />
          </div>

          <div className="p-1 justify-content-center row">
            <button
              className="btn button-search"
              type="button"
              onClick={() => fillContentInf()}
            >
              Buscar
            </button>
          </div>
        </div>

        <div className="col-7 pt-3 p-1 securtiy-container-search">
          <p className="m-0 title-search">Informacion</p>
          <div className="col-12 d-flex aling-items-center p-1">
            <div className="col-6 pl-4 flex-column d-flex justify-content-center aling-items-center">
              <div className="d-flex">
                <p className="m-0 mr-2 txt-information-sub">Nombre: </p>
                <p className="m-0 txt-information-value">
                  {information.fullname}
                </p>
              </div>
              <div className="d-flex">
                <p className="m-0 mr-2 txt-information-sub">Cedula:</p>
                <p className="m-0 txt-information-value">
                  {information.documentid}
                </p>
              </div>
              <div className="d-flex">
                <p className="m-0 mr-2 txt-information-sub">Telefono:</p>
                <p className="m-0 txt-information-value">
                  {information.phonenumber}
                </p>
              </div>
            </div>

            <div className="col-6 pl-4 flex-column d-flex justify-content-center aling-items-center">
              <div className="d-flex">
                <p className="m-0 mr-2 txt-information-sub">Email:</p>
                <p className="m-0 txt-information-value">{information.email}</p>
              </div>
              <div className="d-flex">
                <p className="m-0 mr-2 txt-information-sub">Calle:</p>
                <p className="m-0 txt-information-value">
                  {information.street}
                </p>
              </div>
              <div className="d-flex">
                <p className="m-0 mr-2 txt-information-sub">Numero de Casa:</p>
                <p className="m-0 txt-information-value">{information.house}</p>
              </div>
            </div>
          </div>
        </div>

        <div className="col-12 align-self-start">
          {/* <DataGrip /> */}

          <div className="col-12 d-flex justify-content-center align-self-center datagrid-container">
            <table className="datagrid-table">
              <thead>
                <tr className="datagrid-head">
                  <th id="num-head">No.</th>
                  <th>Mes</th>
                  <th>Cuenta</th>
                  <th>Paga</th>
                  <th>Deuda</th>
                  <th>Comentario</th>
                  <th>Responsable</th>
                  <th>Editar</th>
                  <th>Pagar</th>
                </tr>
              </thead>

              <tbody>
                <tr className="datagrid-body">
                  <td id="num-body">1</td>
                  <td>January</td>
                  <td>700</td>
                  <td>0</td>
                  <td>700</td>
                  <td></td>
                  <td id="txt-comment">Click Enter Comment...</td>
                  <td>
                    <button type="button" className="btn btn-warning">
                      Edit
                    </button>
                  </td>
                  <td>
                    <button type="button" className="btn btn-primary">
                      To Pay
                    </button>
                  </td>
                </tr>
                <tr className="datagrid-body">
                  <td id="num-body">2</td>
                  <td>January</td>
                  <td>700</td>
                  <td>0</td>
                  <td>700</td>
                  <td></td>
                  <td id="txt-comment">Click Enter Comment...</td>
                  <td>
                    <button type="button" className="btn btn-warning">
                      Edit
                    </button>
                  </td>
                  <td>
                    <button type="button" className="btn btn-primary">
                      To Pay
                    </button>
                  </td>
                </tr>
                <tr className="datagrid-body">
                  <td id="num-body">3</td>
                  <td>January</td>
                  <td>700</td>
                  <td>0</td>
                  <td>700</td>
                  <td></td>
                  <td id="txt-comment">Click Enter Comment...</td>
                  <td>
                    <button type="button" className="btn btn-warning">
                      Edit
                    </button>
                  </td>
                  <td>
                    <button type="button" className="btn btn-primary">
                      To Pay
                    </button>
                  </td>
                </tr>
                <tr className="datagrid-body">
                  <td id="num-body">4</td>
                  <td>January</td>
                  <td>700</td>
                  <td>0</td>
                  <td>700</td>
                  <td></td>
                  <td id="txt-comment">Click Enter Comment...</td>
                  <td>
                    <button type="button" className="btn btn-warning">
                      Edit
                    </button>
                  </td>
                  <td>
                    <button type="button" className="btn btn-primary">
                      To Pay
                    </button>
                  </td>
                </tr>
                <tr className="datagrid-body">
                  <td id="num-body">5</td>
                  <td>January</td>
                  <td>700</td>
                  <td>0</td>
                  <td>700</td>
                  <td></td>
                  <td id="txt-comment">Click Enter Comment...</td>
                  <td>
                    <button type="button" className="btn btn-warning">
                      Edit
                    </button>
                  </td>
                  <td>
                    <button type="button" className="btn btn-primary">
                      To Pay
                    </button>
                  </td>
                </tr>
                <tr className="datagrid-body">
                  <td id="num-body">6</td>
                  <td>January</td>
                  <td>700</td>
                  <td>0</td>
                  <td>700</td>
                  <td></td>
                  <td id="txt-comment">Click Enter Comment...</td>
                  <td>
                    <button type="button" className="btn btn-warning">
                      Edit
                    </button>
                  </td>
                  <td>
                    <button type="button" className="btn btn-primary">
                      To Pay
                    </button>
                  </td>
                </tr>
                <tr className="datagrid-body">
                  <td id="num-body">7</td>
                  <td>January</td>
                  <td>700</td>
                  <td>0</td>
                  <td>700</td>
                  <td></td>
                  <td id="txt-comment">Click Enter Comment...</td>
                  <td>
                    <button type="button" className="btn btn-warning">
                      Edit
                    </button>
                  </td>
                  <td>
                    <button type="button" className="btn btn-primary">
                      To Pay
                    </button>
                  </td>
                </tr>
                <tr className="datagrid-body">
                  <td id="num-body">8</td>
                  <td>January</td>
                  <td>700</td>
                  <td>0</td>
                  <td>700</td>
                  <td></td>
                  <td id="txt-comment">Click Enter Comment...</td>
                  <td>
                    <button type="button" className="btn btn-warning">
                      Edit
                    </button>
                  </td>
                  <td>
                    <button type="button" className="btn btn-primary">
                      To Pay
                    </button>
                  </td>
                </tr>
                <tr className="datagrid-body">
                  <td id="num-body">9</td>
                  <td>January</td>
                  <td>700</td>
                  <td>0</td>
                  <td>700</td>
                  <td></td>
                  <td id="txt-comment">Click Enter Comment...</td>
                  <td>
                    <button type="button" className="btn btn-warning">
                      Edit
                    </button>
                  </td>
                  <td>
                    <button type="button" className="btn btn-primary">
                      To Pay
                    </button>
                  </td>
                </tr>
                <tr className="datagrid-body">
                  <td id="num-body">10</td>
                  <td>January</td>
                  <td>700</td>
                  <td>0</td>
                  <td>700</td>
                  <td></td>
                  <td id="txt-comment">Click Enter Comment...</td>
                  <td>
                    <button type="button" className="btn btn-warning">
                      Edit
                    </button>
                  </td>
                  <td>
                    <button type="button" className="btn btn-primary">
                      To Pay
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Index;
