import React from 'react'

const CreateUserForm = () => {
    return (
        <>
            <div class="row">
                <div className="col">
                    <input
                        className="logup-input"
                        name="username"
                        type="text"
                        placeholder="Username"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    <input
                        className="logup-input"
                        name="password"
                        type=""
                        placeholder="Password"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    <input
                        className="logup-input"
                        name="Type"
                        type="list"
                        placeholder="Type Of Housing"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>

                <div className="w-100 mt-5"></div>

                <div className="col">
                    <input
                        className="logup-input"
                        name="street"
                        type="text"
                        placeholder="Street"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    <input
                        className="logup-input"
                        name="block"
                        type="text"
                        placeholder="Block"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
                <div className="col">
                    <input
                        className="logup-input"
                        name="Housenumber"
                        type="number"
                        placeholder="House Number"
                        minLength="4"
                        maxLength="16"
                        required
                    />
                </div>
            </div>
        </>
    )
}

export default CreateUserForm;
