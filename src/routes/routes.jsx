import React, { useContext } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import GlobalContext from "../context/GlobalContext";
import PrivateRoutes from "../routes/Private.Routes";
import Login from "../components/LogIn/Login";
import Header from "../common/components/Header/Header";
import Dashboard from "../components//Dashboard/Dashboard";
import PaySecurity from "../components/PaySecurity/PaySecurity";

const Routes = () => {
  const [contextState] = useContext(GlobalContext);

  return (
    <BrowserRouter>
      <Header />
      <Switch>
        <Route
          exact
          path={`${process.env.REACT_APP_RUTE}/`}
          component={contextState.token ? Dashboard : Login}
        />
        <PrivateRoutes
          exct
          path={`${process.env.REACT_APP_RUTE}/dashboard`}
          component={contextState.token ? Dashboard : Login}
        />
        <Route
          path={`${process.env.REACT_APP_RUTE}/paysecurity`}
          component={contextState.token ? PaySecurity : Login}
        />

        {/* <Route
          exact
          path={`${process.env.REACT_APP_RUTE}*`}
          component={Page404}
        /> */}
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
