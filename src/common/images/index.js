import logouser from './user3.svg';
import employee from './chris_bailey-08429_lores-e1641908639897-1022x800.jpg';
import iconPay from './pay.png';
import iconPayCar from './money.png';
import report from './business-report.png';
import iconUser from '../images/user-profile.png';
import login from '../images/login (1).svg';
import community from '../images/community.svg';
import flagUs from '../images/united-states.png';
import flagSpain from '../images/spain.png';


const Images = {
    logouser,
    iconPay,
    iconPayCar,
    report,
    document,
    iconUser,
    employee,
    login,
    community,
    flagUs,
    flagSpain
}

export default Images;