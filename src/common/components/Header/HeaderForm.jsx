import React, { useContext } from "react";
import Images from "../../../common/images";
import GlobalContext from "../../../context/GlobalContext";
import { Link } from "react-router-dom";

const HeaderForm = ({ logOut }) => {
  const [contextState] = useContext(GlobalContext);
  return (
    <>
      <header className="header-container">
        <div className="header-grid">
          <div className="header-grid-logo">
            <Link to="">
              <div className="">
                <img className="img-community" src={Images.community} alt="" />
              </div>
            </Link>
          </div>
          <div className="header-grid-menu"></div>
          <div className="header-grid-user">
            {" "}
            {contextState.token ? (
              <div className="header-user-icon-cont">
                <img
                  className="header-user-icon"
                  src={contextState.photo}
                  onClick={() => logOut()}
                  type="button"
                  alt=""
                />
                <p onClick={() => logOut()} className="header-user-text">
                  Hola, {contextState.fullName.split(" ")[0]}
                </p>
              </div>
            ) : null}
          </div>
        </div>
      </header>
    </>
  );
};

export default HeaderForm;
