import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import GlobalContext from "../../../context/GlobalContext";
import HeaderForm from "./HeaderForm";

const Header = () => {
  const history = useHistory();
  const [, , contextMiddleware] = useContext(GlobalContext);

  const logOut = () => {
    contextMiddleware.signOut();
    history.push("/");
  };

  return (
    <>
      <HeaderForm logOut={logOut} />
    </>
  );
};

export default Header;
