import React from 'react'

const Button = (btnClass, btnName, btnType, btnHandele) => {
    return (
        <>
            <div className="d-flex justify-content-center mt-4">
                <button className={btnClass} name={btnName} type={btnType} onClick={btnHandele}></button>
            </div>
        </>
    )
}

export default Button
