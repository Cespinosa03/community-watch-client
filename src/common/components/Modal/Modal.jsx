import React from 'react'
import Portal from '../../../utils/Portal'
import Input from '../Input/Input'

const Modal = ({ children, modalToggle, modalActive, modalTitle }) => {
    return (
        <Portal>
            {modalActive && (
                <div className='wrapper'>
                    <div className='window'>
                        <div className='title'>{modalTitle}</div>
                        <Input />
                        <Input />
                        <Input />
                        <Input />
                        <div>{children}</div>
                    </div>
                    <div className="background" onClick={modalToggle}></div>
                </div>
            )}
        </Portal>
    )
}

export default Modal
